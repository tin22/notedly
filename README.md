# Notedly #

Project with frontend and backend.

### Requirements ###

* Users will be able to create notes as well as read, update, and delete the notes they’ve created.
* Users will be able to view a feed of notes created by other users, and read individual notes created by others, though they will not be able to update or delete them.
* Users will be able to create an account, login, and logout.
* Users will be able to retrieve their profile information as well as the public profile information of other users.
* Users will able to favorite the notes of other users as well as retrieve a list of their favorites.

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact